﻿Imports IWshRuntimeLibrary
''' <summary>
''' Version 1.3 - 11/2/2018
'''     Summary: Added new directories and subdirectories to the J drive.
'''     
''' Version 1.31 - 11/13/2018
'''     Summary: Changed location of FolderStructure.xlsx
'''         
''' Version 1.31 - 12/07/2018
'''     Summary: - Inserted FolderStructure.xlsx into project data. Program now copies the spreadsheet from the same
'''              directory as the exe. 
'''              - Restructured and reorganized J drive.
''' </summary>
Public Class createProjDirectory
    Public projectId As String
    Dim projectIdArray() As String

    'Dim filePathJ, filePathO As String
    Public filePathJ = "\\HLE-VFS\PROJ\"
    Public filePathO = "\\HLE-VFS\IMAGES\"
    'Location of folder structure spreadsheet. In same folder as program executable.
    Private folderStructureLocation As String = "FolderStructure.xlsx"
    'Location of J Drive proposals folder
    Private jDriveProposalFolder As String = filePathJ & "PROPOSALS\"

    'Only one template can be selected
    Private Sub hleCheck_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles hleCheck.Click
        If hleCheck.Checked = True Then
            fdotCheck.Checked = False
        End If
    End Sub
    Private Sub fdotCheck_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles fdotCheck.Click
        If fdotCheck.Checked = True Then
            hleCheck.Checked = False
        End If
    End Sub
    'choose Drives to create directories on
    Private Sub jCheck_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles jCheck.Click
        If jCheck.Checked = True Then
            filePathJ = "\\HLE-VFS\PROJ\"
        End If
    End Sub
    Private Sub oCheck_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles oCheck.Click
        If oCheck.Checked = True Then
            filePathO = "\\HLE-VFS\IMAGES\"
        End If
    End Sub
    'Create specified directories
    Private Sub createButton_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles createButton.Click
        Dim folderName As String = ""
        Dim fldrStrLegal As Boolean = True
        Dim jDriveMsg As String = ""
        Dim oDriveMsg As String = ""
        Dim oDriveCreated As Boolean = False
        Dim jDriveCreated As Boolean = False

        If projNumTxt.Text = "" Or projNumTxt.Text = "Field entry is required to continue" Or projNumTxt.Text = "Invalid character or string" Then
            projNumTxt.Text = "Field entry is required to continue"
            Exit Sub
        Else
            folderName = projNumTxt.Text
            'Handle illegal Characters and Strings
            fldrStrLegal = legalFldrName(folderName)
            If fldrStrLegal = False Then
                projNumTxt.Text = "Invalid character or string"
                Exit Sub
            End If
        End If

        If jCheck.Checked = True Then
            'does the file exist
            Dim jDriveFolder = filePathJ & folderName
            If Not (System.IO.Directory.Exists(jDriveFolder)) Then
                MkDir(jDriveFolder)

                'Copies folder structure spreadsheet to highest J Drive directory
                My.Computer.FileSystem.CopyFile(folderStructureLocation, jDriveFolder & "\FolderStructure.xlsx")

                'If user has entered multiple IDs separated by commas, creates an array from them. Otherwise uses single ID or "\00000001" as default
                If Not String.IsNullOrEmpty(projectIdTxt.Text) And projectIdTxt.Text.Contains(",") Then
                    projectIdArray = Split(projectIdTxt.Text, ",")
                ElseIf Not String.IsNullOrEmpty(projectIdTxt.Text) Then
                    projectIdArray = {projectIdTxt.Text.Trim}
                Else
                    projectIdArray = {"0000001"}
                End If

                'Creates a shortcut to the J Drive Proposals folder
                CreateShortcut(jDriveFolder & "\Proposals (J Drive).lnk", jDriveProposalFolder)

                For i As Integer = 0 To (projectIdArray.Length - 1)
                    projectId = "\" & projectIdArray(i).Trim
                    Dim jProjectIdFolder As Object = jDriveFolder & projectId

                    MkDir(jProjectIdFolder)

                    MkDir(jProjectIdFolder & "\OLD")

                    MkDir(jProjectIdFolder & "\01-ConceptReport")
                    MkDir(jProjectIdFolder & "\01-ConceptReport" & "\01-TeamMeeting")
                    MkDir(jProjectIdFolder & "\01-ConceptReport" & "\02-Alternative#")
                    MkDir(jProjectIdFolder & "\01-ConceptReport" & "\03-VEStudy")

                    MkDir(jProjectIdFolder & "\02-DesignDataBook")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\01-Drainage")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\02-PavementDesign")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\03-Quantities")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\03-Quantities" & "\00-BridgeQtys")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\03-Quantities" & "\01-Calcs")
                    MkDir(jProjectIdFolder & "\02-DesignDataBook" & "\03-Quantities" & "\02-DGN")

                    MkDir(jProjectIdFolder & "\03-DGN")

                    MkDir(jProjectIdFolder & "\04-InroadsData")
                    MkDir(jProjectIdFolder & "\04-InroadsData" & "\" & projectId)

                    MkDir(jProjectIdFolder & "\07-Environmental")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\01-Screening")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\02-4f")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\03-Section7")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\04-PublicInvolvement")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\04-PublicInvolvement" & "\01-PIOH")
                    MkDir(jProjectIdFolder & "\07-Environmental" & "\04-PublicInvolvement" & "\02-StakeholderMeetings")

                    MkDir(jProjectIdFolder & "\08-HD")
                    MkDir(jProjectIdFolder & "\08-HD" & "\01-FEMAData")
                    MkDir(jProjectIdFolder & "\08-HD" & "\02-HEC-RAS")
                    MkDir(jProjectIdFolder & "\08-HD" & "\03-Models")
                    MkDir(jProjectIdFolder & "\08-HD" & "\04-Report")

                    MkDir(jProjectIdFolder & "\09-Bridge" & "\01-Concept")

                    MkDir(jProjectIdFolder & "\09-Bridge" & "\02-Preliminary")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\02-Preliminary" & "\01-DeckDrainageDesign")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\02-Preliminary" & "\02-BermElev")

                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\01-Geometry")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\02-BeamSeatElev")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\03-DeckDesign")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\04-BeamDesign")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\05-BearingDesign")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\06-BentDesign")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\03-Final" & "\07-Quantities")

                    MkDir(jProjectIdFolder & "\09-Bridge" & "\04-ConstEng")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\04-ConstEng" & "\01-GirderErection")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\04-ConstEng" & "\02-BeamBracing")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\04-ConstEng" & "\03-DeckOverhang")
                    MkDir(jProjectIdFolder & "\09-Bridge" & "\04-ConstEng" & "\04-Shoring")

                    MkDir(jProjectIdFolder & "\09-Bridge" & "\05-As-Builts")

                    MkDir(jProjectIdFolder & "\10-Struct")

                    MkDir(jProjectIdFolder & "\11-Wall")
                    MkDir(jProjectIdFolder & "\11-Wall" & "\01-Concept")
                    MkDir(jProjectIdFolder & "\11-Wall" & "\02-Preliminary")

                    MkDir(jProjectIdFolder & "\11-Wall" & "\03-Final")
                    MkDir(jProjectIdFolder & "\11-Wall" & "\03-Final" & "\01-Calculations")
                Next

                MkDir(jDriveFolder & "\Admin")

                MkDir(jDriveFolder & "\Admin" & "\01-Communications")

                MkDir(jDriveFolder & "\Admin" & "\02-Contract")
                MkDir(jDriveFolder & "\Admin" & "\02-Contract" & "\Sub#1Contract")

                MkDir(jDriveFolder & "\Admin" & "\03-CostProposal")
                MkDir(jDriveFolder & "\Admin" & "\03-CostProposal" & "\Sub#1Contract")

                MkDir(jDriveFolder & "\Admin" & "\04-Invoicing")
                MkDir(jDriveFolder & "\Admin" & "\04-Invoicing" & "\Sub#1Invoices")
                MkDir(jDriveFolder & "\Admin" & "\04-Invoicing" & "\YYYY-MM-DD")

                MkDir(jDriveFolder & "\Admin" & "\05-Schedule")

                MkDir(jDriveFolder & "\Admin" & "\06-ProjectManagement")
                MkDir(jDriveFolder & "\Admin" & "\06-ProjectManagement" & "\01-GDOTMonthlyProjectHistoryReport")
                MkDir(jDriveFolder & "\Admin" & "\06-ProjectManagement" & "\02-GDOTMonthlyStatusMeeting")
                MkDir(jDriveFolder & "\Admin" & "\06-ProjectManagement" & "\03-InternalReporting")

                MkDir(jDriveFolder & "\Admin" & "\07-QualityManagement")
                MkDir(jDriveFolder & "\Admin" & "\07-QualityManagement" & "\01-QMT")
                MkDir(jDriveFolder & "\Admin" & "\07-QualityManagement" & "\02-QA")
                MkDir(jDriveFolder & "\Admin" & "\07-QualityManagement" & "\03-QualityCertifications")

                jDriveCreated = True
                jDriveMsg = "J Drive Project Created!"
            Else
                jDriveMsg = "J Drive project directory already exists"
            End If
        End If

        If oCheck.Checked = True Then
            'does the file exist
            Dim oDriveFolder = filePathO & folderName
            If Not (System.IO.Directory.Exists(oDriveFolder)) Then
                MkDir(oDriveFolder)

                'Copies folder structure spreadsheet to highest J Drive directory
                My.Computer.FileSystem.CopyFile(folderStructureLocation, oDriveFolder & "\FolderStructure.xlsx")

                MkDir(oDriveFolder & "\01-RasterImagery")

                MkDir(oDriveFolder & "\02-Data")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\01-GDOT")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\01-GDOT" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\02-OtherAgency")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\02-OtherAgency" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\03-Subconsultant")
                MkDir(oDriveFolder & "\02-Data" & "\01-DataReceived" & "\03-Subconsultant" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\01-GDOT")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\01-GDOT" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\02-OtherAgency")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\02-OtherAgency" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\03-Subconsultant")
                MkDir(oDriveFolder & "\02-Data" & "\02-DataSent" & "\03-Subconsultant" & "\yyyymmdd-Desc")

                MkDir(oDriveFolder & "\03-Photos")
                MkDir(oDriveFolder & "\03-Photos" & "\01-Preconstruction")
                MkDir(oDriveFolder & "\03-Photos" & "\01-Preconstruction" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\03-Photos" & "\02-Construction")
                MkDir(oDriveFolder & "\03-Photos" & "\02-Construction" & "\yyyymmdd-Desc")
                MkDir(oDriveFolder & "\03-Photos" & "\03-Final")
                MkDir(oDriveFolder & "\03-Photos" & "\03-Final" & "\yyyymmdd-Desc")

                MkDir(oDriveFolder & "\04-RecordDocuments")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\01-ConceptReport")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\01-ConceptReport" & "\01-TeamMeeting")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\01-ConceptReport" & "\02-VEStudy")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\02-DesignDataBook")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\03-Traffic")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\04-Utilities")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\05-Drainage")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\06-PavementDesign")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\07-Environmental")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\07-Environmental" & "\01-PublicInvolvement")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\07-Environmental" & "\01-PublicInvolvement" & "\01-PIOH")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\07-Environmental" & "\01-PublicInvolvement" & "\02-StakeholderMeetings")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\08-HD")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\09-PFPRPackage")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\09-PFPRPackage" & "\01-Plans")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\09-PFPRPackage" & "\02-Checklists")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\09-PFPRPackage" & "\03-SpecialProvisions")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\09-PFPRPackage" & "\04-ReviewDocuments")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\10-ROWPackage")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\11-RailroadPackage")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\12-FFPRPackage")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\12-FFPRPackage" & "\01-Plans")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\12-FFPRPackage" & "\02-Checklists")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\12-FFPRPackage" & "\03-SpecialProvisions")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\12-FFPRPackage" & "\04-ReviewDocuments")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\13-FinalPackage")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\13-FinalPackage" & "\01-Plans")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\13-FinalPackage" & "\02-Checklists")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\13-FinalPackage" & "\03-SpecialProvisions")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\13-FinalPackage" & "\04-ReviewDocuments")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#" & "\01-Concept")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#" & "\02-Preliminary")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#" & "\03-Final")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#" & "\04-ReviewDocuments")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\14-Bridge" & "\Bridge#" & "\05-ShopDrawings")

                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#" & "\01-Concept")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#" & "\02-Preliminary")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#" & "\03-Final")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#" & "\04-ReviewDocuments")
                MkDir(oDriveFolder & "\04-RecordDocuments" & "\15-Wall" & "\Wall#" & "\05-ShopDrawings")

                MkDir(oDriveFolder & "\05-WorkingFiles")

                MkDir(oDriveFolder & "\06-Newforma")
                MkDir(oDriveFolder & "\06-Newforma" & "\01-Emails")
                MkDir(oDriveFolder & "\06-Newforma" & "\02-Transfers")
                MkDir(oDriveFolder & "\06-Newforma" & "\03-Transmittals")
                MkDir(oDriveFolder & "\06-Newforma" & "\04-RecordDocuments")

                oDriveCreated = True
                oDriveMsg = "O Drive Project Created!"
            Else
                oDriveMsg = "O Drive project directory already exists"
            End If
        End If

        'Creates a shortcut to the other folder (from o drive to j drive, or vice-versa) and places it in highest directory.
        If (oDriveCreated And jDriveCreated) Then
            Dim oDriveLocation As String = filePathO & folderName
            Dim jDriveLocation As String = filePathJ & folderName

            CreateShortcut(oDriveLocation & "\" & folderName & " (J Drive).lnk", jDriveLocation)
            CreateShortcut(jDriveLocation & "\" & folderName & " (O Drive).lnk", oDriveLocation)
        End If
        MsgBox(jDriveMsg & Chr(13) & oDriveMsg)
    End Sub
    'Close Form

    Private Sub CreateShortcut(shortcutFullPath As String, target As String)
        Dim wsh As New WshShell()
        Dim newShortcut = DirectCast(wsh.CreateShortcut(shortcutFullPath), IWshRuntimeLibrary.WshShortcut)
        newShortcut.TargetPath = target
        newShortcut.Save()
    End Sub

    Private Sub closeButton_Click(ByVal sender As System.Object, ByVal e As _
        System.EventArgs) Handles closeButton.Click
        Me.Close()
    End Sub

    Private Sub createProjDirectory_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub projectDescriptionLabel_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub projectID_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub projNumLabel_Click(sender As Object, e As EventArgs) Handles projNumLabel.Click

    End Sub
End Class
