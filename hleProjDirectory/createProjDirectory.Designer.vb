﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class createProjDirectory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(createProjDirectory))
        Me.projNumLabel = New System.Windows.Forms.Label()
        Me.projNumTxt = New System.Windows.Forms.TextBox()
        Me.locLabel = New System.Windows.Forms.Label()
        Me.jCheck = New System.Windows.Forms.CheckBox()
        Me.oCheck = New System.Windows.Forms.CheckBox()
        Me.tempLabel = New System.Windows.Forms.Label()
        Me.hleCheck = New System.Windows.Forms.CheckBox()
        Me.fdotCheck = New System.Windows.Forms.CheckBox()
        Me.createButton = New System.Windows.Forms.Button()
        Me.closeButton = New System.Windows.Forms.Button()
        Me.projectIdLabel = New System.Windows.Forms.Label()
        Me.projectIdTxt = New System.Windows.Forms.TextBox()
        Me.projectIdDescription = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'projNumLabel
        '
        Me.projNumLabel.AutoSize = True
        Me.projNumLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.projNumLabel.Location = New System.Drawing.Point(12, 12)
        Me.projNumLabel.Name = "projNumLabel"
        Me.projNumLabel.Size = New System.Drawing.Size(59, 20)
        Me.projNumLabel.TabIndex = 0
        Me.projNumLabel.Text = "H&&L #"
        '
        'projNumTxt
        '
        Me.projNumTxt.Location = New System.Drawing.Point(110, 12)
        Me.projNumTxt.Name = "projNumTxt"
        Me.projNumTxt.Size = New System.Drawing.Size(173, 20)
        Me.projNumTxt.TabIndex = 1
        '
        'locLabel
        '
        Me.locLabel.AutoSize = True
        Me.locLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.locLabel.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.locLabel.Location = New System.Drawing.Point(13, 101)
        Me.locLabel.Name = "locLabel"
        Me.locLabel.Size = New System.Drawing.Size(70, 17)
        Me.locLabel.TabIndex = 5
        Me.locLabel.Text = "Location"
        '
        'jCheck
        '
        Me.jCheck.AutoSize = True
        Me.jCheck.Checked = True
        Me.jCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.jCheck.Location = New System.Drawing.Point(110, 102)
        Me.jCheck.Name = "jCheck"
        Me.jCheck.Size = New System.Drawing.Size(59, 17)
        Me.jCheck.TabIndex = 6
        Me.jCheck.Text = "J Drive"
        Me.jCheck.UseVisualStyleBackColor = True
        '
        'oCheck
        '
        Me.oCheck.AutoSize = True
        Me.oCheck.Checked = True
        Me.oCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.oCheck.Location = New System.Drawing.Point(175, 102)
        Me.oCheck.Name = "oCheck"
        Me.oCheck.Size = New System.Drawing.Size(62, 17)
        Me.oCheck.TabIndex = 7
        Me.oCheck.Text = "O Drive"
        Me.oCheck.UseVisualStyleBackColor = True
        '
        'tempLabel
        '
        Me.tempLabel.AutoSize = True
        Me.tempLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tempLabel.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.tempLabel.Location = New System.Drawing.Point(13, 118)
        Me.tempLabel.Name = "tempLabel"
        Me.tempLabel.Size = New System.Drawing.Size(75, 17)
        Me.tempLabel.TabIndex = 8
        Me.tempLabel.Text = "Template"
        '
        'hleCheck
        '
        Me.hleCheck.AutoSize = True
        Me.hleCheck.Checked = True
        Me.hleCheck.CheckState = System.Windows.Forms.CheckState.Checked
        Me.hleCheck.Enabled = False
        Me.hleCheck.Location = New System.Drawing.Point(110, 119)
        Me.hleCheck.Name = "hleCheck"
        Me.hleCheck.Size = New System.Drawing.Size(50, 17)
        Me.hleCheck.TabIndex = 9
        Me.hleCheck.Text = "HLE "
        Me.hleCheck.UseVisualStyleBackColor = True
        '
        'fdotCheck
        '
        Me.fdotCheck.AutoSize = True
        Me.fdotCheck.Enabled = False
        Me.fdotCheck.Location = New System.Drawing.Point(175, 118)
        Me.fdotCheck.Name = "fdotCheck"
        Me.fdotCheck.Size = New System.Drawing.Size(55, 17)
        Me.fdotCheck.TabIndex = 10
        Me.fdotCheck.Text = "FDOT"
        Me.fdotCheck.UseVisualStyleBackColor = True
        '
        'createButton
        '
        Me.createButton.Location = New System.Drawing.Point(110, 146)
        Me.createButton.Name = "createButton"
        Me.createButton.Size = New System.Drawing.Size(88, 26)
        Me.createButton.TabIndex = 11
        Me.createButton.Text = "Create Project"
        Me.createButton.UseVisualStyleBackColor = True
        '
        'closeButton
        '
        Me.closeButton.Location = New System.Drawing.Point(204, 146)
        Me.closeButton.Name = "closeButton"
        Me.closeButton.Size = New System.Drawing.Size(88, 26)
        Me.closeButton.TabIndex = 12
        Me.closeButton.Text = "Close"
        Me.closeButton.UseVisualStyleBackColor = True
        '
        'projectIdLabel
        '
        Me.projectIdLabel.AutoSize = True
        Me.projectIdLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.projectIdLabel.Location = New System.Drawing.Point(12, 43)
        Me.projectIdLabel.Name = "projectIdLabel"
        Me.projectIdLabel.Size = New System.Drawing.Size(51, 20)
        Me.projectIdLabel.TabIndex = 2
        Me.projectIdLabel.Text = "P.I. #"
        '
        'projectIdTxt
        '
        Me.projectIdTxt.Location = New System.Drawing.Point(110, 43)
        Me.projectIdTxt.Name = "projectIdTxt"
        Me.projectIdTxt.Size = New System.Drawing.Size(173, 20)
        Me.projectIdTxt.TabIndex = 3
        '
        'projectIdDescription
        '
        Me.projectIdDescription.AllowDrop = True
        Me.projectIdDescription.Location = New System.Drawing.Point(107, 66)
        Me.projectIdDescription.Name = "projectIdDescription"
        Me.projectIdDescription.Size = New System.Drawing.Size(163, 27)
        Me.projectIdDescription.TabIndex = 4
        Me.projectIdDescription.Text = "To enter multiple project IDs, separate them by commas."
        '
        'createProjDirectory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(304, 181)
        Me.Controls.Add(Me.projectIdDescription)
        Me.Controls.Add(Me.projectIdTxt)
        Me.Controls.Add(Me.projectIdLabel)
        Me.Controls.Add(Me.closeButton)
        Me.Controls.Add(Me.createButton)
        Me.Controls.Add(Me.fdotCheck)
        Me.Controls.Add(Me.hleCheck)
        Me.Controls.Add(Me.tempLabel)
        Me.Controls.Add(Me.oCheck)
        Me.Controls.Add(Me.jCheck)
        Me.Controls.Add(Me.locLabel)
        Me.Controls.Add(Me.projNumTxt)
        Me.Controls.Add(Me.projNumLabel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "createProjDirectory"
        Me.Text = "Create Directory"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents projNumLabel As System.Windows.Forms.Label
    Friend WithEvents projNumTxt As System.Windows.Forms.TextBox
    Friend WithEvents locLabel As System.Windows.Forms.Label
    Friend WithEvents jCheck As System.Windows.Forms.CheckBox
    Friend WithEvents oCheck As System.Windows.Forms.CheckBox
    Friend WithEvents tempLabel As System.Windows.Forms.Label
    Friend WithEvents hleCheck As System.Windows.Forms.CheckBox
    Friend WithEvents fdotCheck As System.Windows.Forms.CheckBox
    Friend WithEvents createButton As System.Windows.Forms.Button
    Friend WithEvents closeButton As System.Windows.Forms.Button
    Friend WithEvents projectIdLabel As Label
    Friend WithEvents projectIdTxt As TextBox
    Friend WithEvents projectIdDescription As Label
End Class
