﻿Module fldrNameStr
    Public Function legalFldrName(strToCheck As String) As Boolean
        Dim illegalCharacters() As String = {"~", "#", "%", "&", "*", "{", "}", "\", ":", "<", ">", "?", "/", "|", Chr(34)}
        Dim illegalStrings() As String = {".files", "_files", "-Dateien", "_fichiers", "_bestanden", "_file", "_archivos", _
                                          "-filer", "_tiedostot", "_pliki", "_soubory", "_elemei", "_ficheiros", "_arquivos", _
                                          "_dosyalar", "_datoteke", "_fitxers", "_failid", "_fails", "_bylos", "_fajlovi", "_fitxategiak"}
        Dim i As Integer = 0
        Dim isLegalChar = True
        Dim isLegalStr = True
        legalFldrName = True



        'Check for illegal characters
        For Each illegalCharacter As String In illegalCharacters
            i = InStr(strToCheck, illegalCharacter)
            If i > 0 Then
                isLegalChar = False
                If isLegalChar = False Then
                    Exit For
                End If
            End If
        Next
        'Check for illegal Strings
        i = 0
        For Each illegalString As String In illegalStrings
            i = InStr(strToCheck, illegalString)
            If i > 0 Then
                isLegalStr = False
                If isLegalStr = False Then
                    Exit For
                End If
            End If
        Next
        'return false if folder name contains illegal characters or strings
        If isLegalStr = False Or isLegalChar = False Then
            legalFldrName = False
        End If
    End Function
End Module
